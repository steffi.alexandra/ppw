from django.shortcuts import render
from django.shortcuts import redirect
from .forms import PostForm, LoginForm
from django.http import HttpResponseRedirect
from .models import StatusInput, LoginPage
from django.http import JsonResponse
from django.core import serializers
import urllib.request, json
from google.oauth2 import id_token
from google.auth.transport import requests
from django.urls import reverse
from django.shortcuts import get_object_or_404


# Create your views here.
response = {'author' : 'Steffi Alexandra'}

def profile(request):
    return render(request, 'tutorial_1.html')

def google(request):
    return render(request, 'google.html')

def favorite(request): 
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST.get('id')
        if not 'books' in request.session.keys():
            request.session['books'] = [id]
            size = 1
        else:
            books = request.session['books']
            books.append(id)
            request.session['books'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': id, "items": books})
    if 'books' in request.session.keys():
        listbookSession = request.session['books']
    else:
        listbookSession = []
    nama = request.session['name']
    return render(request, 'books.html', {'count': len(listbookSession), 'nama': nama})

def unfavorite(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST.get('id')
        books = request.session['books']
        books.remove(id)
        size = len(books)
        request.session['books'] = books
        return JsonResponse({'count': size, 'id':id, "books":books})

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            info = id_token.verify_oauth2_token(token, requests.Request(),
                                                  "999533023599-f1b247rlm9b9apj8q1tol9r2so73jjon.apps.googleusercontent.com")
            if info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            user_id = info['sub']
            email = info['email']
            name = info['name']
            request.session['user_id'] = user_id
            request.session['email'] = email
            request.session['name'] = name
            request.session['books'] = []
            return JsonResponse({"status": "0", "url": reverse("books"), "name": name})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'google.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))


def loginpage(request):
    form = LoginForm()
    info = LoginPage.objects.all()
    context = {
        'form' : form,
        'info' : info
    }
    return render(request, 'login_page.html', context)

def subs(request) :
    subs_list = LoginPage.objects.all()
    response['subs_list'] = subs_list
    html = 'resultpage.html'
    return render (request, html, response)

def subs_json(request) :
    subscribe = [obj.as_dict() for obj in LoginPage.objects.all()]
    return JsonResponse ({"results" : subscribe}, content_type = 'application/json')

def unsub(request, val):
    if request.method == 'POST':
        emails = request.POST['email']
        LoginPage.objects.get(email=val).delete()
        return JsonResponse({'done' : True})

def subscribe(request):
    if request.method == "POST":
        response['email'] = request.POST['email']
        response['name'] = request.POST['name']
        response['password'] = request.POST['password']
        credentials = LoginPage(email = response['email'], name=response['name'], password=response['password'])
        credentials.save()
        return JsonResponse({'post' : True})

def checkEmail(request):
    if request.method == "POST":
        emails = request.POST['email']
        login = {
            'invalid': LoginPage.objects.filter(email = emails).exists()
        }
        return JsonResponse(login)

def json_for_book(request, var):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + var
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    return JsonResponse(data)

def getData(request, var):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    url = "https://www.googleapis.com/books/v1/volumes?q=" + var
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    hasil = data['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        if dataId in request.session['books']:
            bool = True
        else:
            bool = False
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],"published": items['volumeInfo']['publishedDate'],'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist, "total":len(request.session['books'])})

def getName(request):
    return JsonResponse({'name':request.session['name']})

def books(request):
    return render(request, 'books.html')

def status(request):
    if request.method == "POST":
        response['status'] = request.POST['status']
        update = StatusInput(status = response['status'])
        update.save()
        form = PostForm()
        stats = StatusInput.objects.all()
        html = 'lab6.html'
        context = {
            'form' : form,
            'stats' : stats
        }
        return render(request, html, context)
    else:
        form = PostForm()
        stats = StatusInput.objects.all()
        html = 'lab6.html'
        context = {
            'form' : form,
            'stats' : stats
        }
        return render(request, html, context)
