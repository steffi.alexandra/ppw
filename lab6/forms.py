from django import forms

from .models import StatusInput, LoginPage


class PostForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    status = forms.CharField(label = 'Status', max_length=300, widget=forms.TextInput(attrs=attrs), required = True )

class LoginForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    attrsPass = {
        'class' : 'form-control',
        'type' : 'password'
    }


    name = forms.CharField(label = 'Full Name', max_length = 100, required = True)
    email = forms.EmailField(label = 'Email', max_length=100, widget=forms.EmailInput(attrs=attrs), required = True)
    password = forms.CharField(label = 'Password :', min_length=8, max_length=20, widget=forms.PasswordInput(attrs=attrsPass), required = True)