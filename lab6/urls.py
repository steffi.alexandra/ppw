from django.urls import path
from .views import status, getName, login, getData, favorite, unfavorite, logout, profile, json_for_book, books, loginpage, checkEmail, subscribe, subs, subs_json, unsub, google

urlpatterns = [
    path('lab6.html', status),
    path('tutorial_1.html', profile),
    path('books.html', books, name = "books"),
    path( 'json_for_book/<var>' , json_for_book, name = "json_for_book"),
    path('login_page.html', loginpage, name = "loginpage"),
    path("subscribe", subscribe, name="subscribe"),
    path('checkEmail', checkEmail, name= "checkEmail"),
    path('resultpage.html', subs),
    path('subs_json/', subs_json, name= "subs_json"),
    path('unsub/<val>', unsub, name = "unsub"),
    path('google.html', google),
    path("login", login, name="login"),
    path("cekFav/<var>", getData, name="cekFav"),
    path("getName", getName, name="getName"),
    path("favorite", favorite, name="favorite"),
    path("unfavorite", unfavorite, name="unfavorite"),
    path("logout", logout, name ="logout"),
    path('', status)
]