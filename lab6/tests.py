from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django_dynamic_fixture import G
from .views import status, profile
from .models import StatusInput
from .forms import PostForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

class Lab6UnitTest(TestCase):

    def test_lab_6_url_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_lab_6_form(self):
        model = G(StatusInput)
        models = StatusInput.objects.all()
        self.assertEqual(models.count(), 1)

    def test_hello_world(self):
        request = HttpRequest()
        response = status(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('hello, apa kabar?', htmlResponse)
    
    def test_image_exists(self):
        request = HttpRequest()
        response = profile(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('img/profile', htmlResponse)

    def test_blank_form_validation(self):
        form = PostForm(data={'status' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = PostForm()
        self.assertIn('class="form-control"', form.as_table())

    def test_status(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['status'] = 'Hello'
        status(request)


#      def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(Story6FunctionalTest,self).setUp()

#     def tearDown(self): #
#         self.selenium.implicitly_wait(3)
#         self.selenium.quit()

#     def test_input_todo(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         isi = self.selenium.find_element_by_id('id_status')
#         isi.send_keys('Halo ini functional test buat lab')
#         isi.submit()
#         time.sleep(10)

#     def test_if_the_input_exists(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         self.assertIn('Halo ini functional test buat lab', self.selenium.page_source)

#     def test_if_the_landing_page_title_exists(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         title = self.selenium.find_element_by_tag_name('h1').text
#         self.assertIn('hello, apa kabar?', title)

#     def test_if_the_page_title_exits(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         judul = self.selenium.title
#         self.assertEqual(judul, '')
    
#     def test_navbar_color(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         bg = self.selenium.find_element_by_id('myTopnav')
#         test = bg.value_of_css_property('background-color')
#         self.assertEquals(test, 'rgba(51, 51, 51, 1)')

#     def test_landing_page_title_color(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/')
#         title = self.selenium.find_element_by_tag_name('h1')
#         border = title.value_of_css_property('margin')
#         self.assertEquals(border, '103.4px')

#     def test_color_change(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com//tutorial_1.html')
#         button = self.selenium.find_element_by_id('change')
#         self.selenium.execute_script("arguments[0].click();", button)
#         self.selenium.execute_script("arguments[0].click();", button)
#         accordions = self.selenium.find_element_by_class_name('flex-item')
#         color = accordions.value_of_css_property('background-color')
#         self.assertEquals(color, 'rgba(255, 255, 255, 1)')

#     def test_stars_count(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/books.html')
#         star = self.selenium.find_element_by_tag_name('button')
#         star.click()
#         amount = self.selenium.find_element_by_id('jum').text
#         self.assertEqual(amount, '1')

#     def test_content_change(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/books.html')
#         change = self.selenium.find_element_by_class_name("cooking")
#         self.selenium.execute_script("arguments[0].click();", change)
#         self.assertTrue("Cooking" in self.selenium.page_source)

#     def test_input_sub(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/login_page.html')
#         name = self.selenium.find_element_by_id("id_name")
#         name.send_keys("Steffi Alexandra")
#         email = self.selenium.find_element_by_id("id_email")
#         email.send_keys("steffialexandraaa@gmail.com")
#         password = self.selenium.find_element_by_id("id_password")
#         password.send_keys("sayasukappw")
#         btn = self.selenium.find_element_by_tag_name("button")
#         self.selenium.execute_script("arguments[0].click();", btn)
#         time.sleep(50)
#         alertMessage = self.selenium.switch_to.alert.text
#         self.assertEqual(alertMessage, "Congratulations, you have successfully subscribed to the website!")

#     def test_sub_list(self):
#         self.selenium.get('http://ppw-f-labenam.herokuapp.com/resultpage.html')
#         self.assertIn('steffialexandraaa@gmail.com', self.selenium.page_source)

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')