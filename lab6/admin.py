from django.contrib import admin
from .models import StatusInput, LoginPage
# Register your models here.

admin.site.register(StatusInput)
admin.site.register(LoginPage)
