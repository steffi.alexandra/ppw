from django.db import models

class StatusInput(models.Model):
    status = models.CharField(max_length=300)

class LoginPage(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    password = models.CharField(max_length=100)

    def as_dict(self) :
        return {
            "name" : self.name,
            "email" : self.email
        }

        